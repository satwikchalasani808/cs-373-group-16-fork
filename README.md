## Link to Website
https://www.care-hub.me/


## Group Information

| Name             | GitLab ID              |
| -------------    | -------------          |
| Neha Desaraju    | @estaudere             |
| Pranav Gupta     | @pranavguptaaa         |
| Rohan Nayak      | @rohan.nayak           |
| Rudy Caballero   | @caballero.rudy        |
| Satwik Chalasani | @satwikchalasani808    |

## Phase Leaders

**Phase Leader Responsibilities**
- Oversee rubric completion
- Lead team meetings
- Check in on team members

| Phase #       | Leader           |
| ------------- | ------------     | 
| One           | Neha Desaraju    | 
| Two           | Satwik Chalasani |  
| Three         | Rudy Caballero   | 
| Four          | Rohan Nayak      | 


## Completion Times

**Phase One**

| Name              | Estimated     | Actual     |
| -------------     | ------------- | ---------- |
| Neha Desaraju     | 13            | 16         |
| Pranav Gupta      | 10            | 13         |
| Rohan Nayak       | 15            | 14         |
| Rudy Caballero    | 15            | 16         |
| Satwik Chalasani  | 12            | 15         |

Postman documentation: [https://documenter.getpostman.com/view/17067225/2sA2xfXYMb](https://documenter.getpostman.com/view/17067225/2sA2xfXYMb)

Youtube link to Pecha Kucha Presentation: https://www.youtube.com/watch?v=Pzh40K25LXc&feature=youtu.be 

Gitlab Pipeline URL: https://gitlab.com/estaudere/cs373-group-16/-/pipelines

Front-End Developers: Pranav Gupta and Rudy Caballero
Back-End Developers: Neha Desaraju, Satwik Chalasani, Rohan Nayak

Leader Role: Facilitated the team in planning and organizing their tasks, ensuring that everyone was on board with the project.

**Phase Two**

| Name              | Estimated     | Actual     |
| -------------     | ------------- | ---------- |
| Neha Desaraju     | 21            | 25         |
| Pranav Gupta      | 12            | 15         |
| Rohan Nayak       | 20            | 23         |
| Rudy Caballero    | 18            | 23         |
| Satwik Chalasani  | 18            | 25         |

**Phase Three**

| Name              | Estimated     | Actual     |
| -------------     | ------------- | ---------- |
| Neha Desaraju     | 25            | 25         |
| Pranav Gupta      | 15            | 25         |
| Rohan Nayak       | 22            | 25         |
| Rudy Caballero    | 23            | 25         |
| Satwik Chalasani  | 24            | 25         |

**Phase Four**

| Name              | Estimated     | Actual     |
| -------------     | ------------- | ---------- |
| Neha Desaraju     | 23            | 20         |
| Pranav Gupta      | 17            | 20         |
| Rohan Nayak       | 24            | 20         |
| Rudy Caballero    | 21            | 20         |
| Satwik Chalasani  | 24            | 20         |

# Git SHA

| Phase #       | Git SHA    								|
| ------------- | ----------------------------------------- | 
| One           | 27f0ac50bc5640400a2da5234d26ba17e334d8f6  | 
| Two           | f99c6e9c707c4d538457d57fbad3817458503abc	|  
| Three         | 2819dfb8415d54a7a0ed34fa2eafaaf724d36e22  |  
| Four          | deebe39c33d6e91f3fe0fa92235829070561ffdb  |  
