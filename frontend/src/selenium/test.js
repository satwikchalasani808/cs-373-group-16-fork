// const { By, Builder } = require("selenium-webdriver");
// const assert = require("assert");
// const chrome = require('selenium-webdriver/chrome');

// const screen = {
//   width: 900,
//   height: 640
// };

// describe("selenium - website renders", () => {

//   it("homepage should load", async function () {
//     const driver = await new Builder().forBrowser("chrome").setChromeOptions(new chrome.Options().addArguments('--headless', '--no-sandbox').windowSize(screen)).build();
//     await driver.get("https://www.care-hub.me/");
//     const title = await driver.findElement(By.tagName("h1")).getText()
//     assert.equal("CareHub", title); // Adjust this to match the actual homepage title
//     if (driver) await driver.quit();
//   }, 60_000);

//   it("about should load", async function () {
//     const driver = await new Builder().forBrowser("chrome").setChromeOptions(new chrome.Options().addArguments('--headless', '--no-sandbox').windowSize(screen)).build();
//     await driver.get("https://www.care-hub.me/about");
//     const title = await driver.findElement(By.tagName("h1")).getText()
//     assert.equal("About Care Hub", title); // Adjust this to match the actual about page title
//     if (driver) await driver.quit();
//   });

//   // Adjust the following tests similarly for types, oncologists, and counties pages
// });

// describe("selenium - links redirect correctly", () => {
//   it("types should redirect to correct instance page", async () => {
//     const driver = await new Builder().forBrowser("chrome").setChromeOptions(new chrome.Options().addArguments('--headless', '--no-sandbox').windowSize(screen)).build();
//     await driver.get("https://www.care-hub.me/types");    
//     // Add your assertions for the redirect behavior here
//     if (driver) await driver.quit();
//   })

//   // Adjust the following tests similarly for counties and oncologists pages
// });

// describe("selenium - home page buttons link correctly", () => {
//   it("home page correctly redirects to types page", async () => {
//     const driver = await new Builder().forBrowser("chrome").setChromeOptions(new chrome.Options().addArguments('--headless', '--no-sandbox').windowSize(screen)).build();
//     await driver.get("https://www.care-hub.me/");  
//     // Add your assertions for the button click behavior here
//     if (driver) await driver.quit();
//   }) 

//   // Adjust the following tests similarly for oncologists and counties buttons
// })
