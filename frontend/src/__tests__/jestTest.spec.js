import {expect, describe, test, it} from 'vitest';
import {render} from '@testing-library/svelte';
import DeveloperCard from "../components/DeveloperCard.svelte";
import HospitalCard from  '../components/HospitalCard.svelte';
import OrgCard from '../components/OrgCard.svelte';
import StateCard from "../components/StateCard.svelte";
import ToolCard from "../components/ToolCard.svelte";

describe('Developer Card component', () => {

    // test 1
    it("checks developer card using name", ()=> {
        const props = {
            name: 'John Doe',
            role: 'Frontend Developer',
            description: 'Lorem ipsum dolor sit amet',
            imageUrl: '/path/to/image.jpg',
            commits: 10,
            issues: 5
        };

        // Render the DeveloperCard component with the defined props
        const {getByText} = render(DeveloperCard, {props});

        // Assert that the component renders with the correct props
        expect(getByText('John Doe')).toBeInTheDocument();
    });

    // test 2
    it("checks developer card using description", ()=> {
        const props = {
            name: 'John Doe',
            role: 'Frontend Developer',
            description: 'Lorem ipsum dolor sit amet',
            imageUrl: '/path/to/image.jpg',
            commits: 10,
            issues: 5
        };

        // Render the DeveloperCard component with the defined props
        const {getByText} = render(DeveloperCard, {props});

        // Assert that the component renders with the correct props
        expect(getByText('Lorem ipsum dolor sit amet')).toBeInTheDocument();
    });

    // test 3
    it("checks developer card using name", ()=> {
        const props = {
            name: 'Peter',
            role: 'Frontend Developer',
            description: 'Lorem ipsum dolor sit amet',
            imageUrl: '/path/to/image.jpg',
            commits: 10,
            issues: 5
        };

        // Render the DeveloperCard component with the defined props
        const {getByText} = render(DeveloperCard, {props});

        // Assert that the component renders with the correct props
        expect(getByText('Peter')).toBeInTheDocument();
    });
});

describe('Org Card component', () => {

    // test 4
    test("checks org card using name", async ()=> {
        const props = {
            name: 'University of Texas',
        };

        // Render the OrgCard component with the defined props
        const {getByText} = render(OrgCard, {props});

        // Assert that the component renders with the correct props
        await expect(getByText('University of Texas')).toBeInTheDocument();
    });

    // test 5
    test("checks org card using name", async ()=> {
        const props = {
            name: 'University of Kentucky',
        };

        // Render the OrgCard component with the defined props
        const {getByText} = render(OrgCard, {props});

        // Assert that the component renders with the correct props
        await expect(getByText('University of Kentucky')).toBeInTheDocument();
    });

    // test 6
    test("checks org card using name", async ()=> {
        const props = {
            name: 'University of California',
        };

        // Render the OrgCard component with the defined props
        const {getByText} = render(OrgCard, {props});

        // Assert that the component renders with the correct props
        await expect(getByText('University of California')).toBeInTheDocument();
    });

    // test 7
    test("checks org card using different name", async ()=> {
        const props = {
            name: 'NORTHWEST COMMUNITY HEALTH CARE',
        };

        // Render the OrgCard component with the defined props
        const {getByText} = render(OrgCard, {props});

        // Assert that the component renders with the correct props
        await expect(getByText('NORTHWEST COMMUNITY HEALTH CARE')).toBeInTheDocument();
    });
});

describe('State Card component', () => {
    // test 8
    test("checks state card using name", ()=> {
        const props = {
            name: 'Texas',
        };

        // Render the StateCard component with the defined props
        const {getByText} = render(StateCard, {props});

        // Assert that the component renders with the correct props
        expect(getByText('Texas')).toBeInTheDocument();
    }); 

    // test 9
    test("checks state card using different name", ()=> {
        const props = {
            name: 'New Jersey',
        };

        // Render the StateCard component with the defined props
        const {getByText} = render(StateCard, {props});

        // Assert that the component renders with the correct props
        expect(getByText('New Jersey')).toBeInTheDocument();
    }); 

    // test 10
    test("checks state card using different name", ()=> {
        const props = {
            name: 'California',
        };

        // Render the StateCard component with the defined props
        const {getByText} = render(StateCard, {props});

        // Assert that the component renders with the correct props
        expect(getByText('California')).toBeInTheDocument();
    }); 
});

// describe('Tool Card component', () => {
//     // test 9
//     it("renders tool card with correct name", ()=> {
//         const props = {
//             imageUrl: '/path/to/image.png',
//             name: 'John Doe'
//         };
    
//         // Render the ToolCard component with the defined props
//         const { getByText } = render(ToolCard, { props });
    
//         // Assert that the component renders with the correct props
//         expect(getByText('John Doe')).toBeInTheDocument();
//     });

//     // test 10
//     it("renders tool card with correct name", async ()=> {
//         const props = {
//             imageUrl: '/path/to/image.png',
//             name: 'Svelte'
//         };

//         // Render the ToolCard component with the defined props
//         const { getByText } = await render(ToolCard, { props });
//         const toolName = getByText(props.name);
//         // Assert that the component renders with the correct props
//         expect(toolName).toBeDefined();
//     });
// });




