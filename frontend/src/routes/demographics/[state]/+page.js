export const prerender = false;

export async function load({fetch, params}) {
    console.log(params.state);
    const res = await fetch(`https://api.care-hub.me/states/` + params.state);
    const data = await res.json();
    if (data) {
      return {
        state: data
      }
    } else {
      return {
        status: 404
      }
    }
}