export const prerender = false;

export async function load({fetch, params}) {
  const res = await fetch(`https://api.care-hub.me/organizations/` + params.name);
  const data = await res.json();
    if (data) {
      return {
        name: data
      }
    } else {
      return {
        status: 404
      }
    }

}