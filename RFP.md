# CareHub

**Group number**: 16

**Team members**: Neha Desaraju, Pranav Gupta, Rohan Nayak, Rudy Caballero, Satwik Chalasani

## Main Info

We are developing a website to highlight the difficulties faced by people in the U.S. who lack health insurance and thus, quality health care. We hope to provide a platform for people to explore more about this issue in the U.S. and raise awareness of the problems that arise with lack of access to healthcare particularly for those who lack health insurance.

The organizations are specifically health centers that provide services to uninsured and low-income individuals (to utilize these services, certain eligibility requirements must be met, such as being low-income or being on government medical assistance such as Medicaid or CHIP). We also hope to provide chargemasters for costs out of pocket at many hospitals, to help inform people who need to consider service options for costs, as they can help to compare nearby hospitals for the costs of services they are looking for (the costs can vary wildly for the same service). Finally, the states page will include certain statistics so that people who want to help or become more aware can learn about how much the problem of uninsured individuals affects their state and county.

## Model information

1. **State Demographic Data** (~50)
   - Attribute 1: Population
   - Attribute 2: Number of hospitals
   - Attribute 3: Number of uninsured individuals
   - Attribute 4: Other demographics (% of low-income individuals)
   - Attribute 5: Other demographics (% of individuals with chronic conditions)
   - Media 1: Bar graph of uninsured individuals per county
   - Media 2: Map of hospitals *(connects to hospital instances)* and helping organizations *(connects to organization instances)*
   - Data: [census.gov](https://www.census.gov/data/developers/data-sets/Health-Insurance-Statistics.html) (REST API), [hhs.gov](https://aspe.hhs.gov/reports/state-local-estimates-uninsured-population-2022), [hrsa.gov](https://data.hrsa.gov/data/download)
2. **Hospitals** (~150)
   - Attribute 1: City
   - Attribute 2: State *(connects to state instances)*
   - Attribute 3: Chargemaster available
   - Attribute 4: Number of beds
   - Attribute 5: Number of uninsured patients
   - Media 1: Chargemaster table (if available)
   - Media 2: Picture of hospital
   - List of closest organizations *(connects to organization instances)*
   - Data: [CMS.gov](https://data.cms.gov/provider-data/topics/hospitals), [healthdata.gov](https://healthdata.gov/State/Hospital-Chargemasters/7nk4-2f7y/about_data)
3. **Organizations** (~200)
   - Attribute 1: City
   - Attribute 2: State *(connects to state instances)*
   - Attribute 3: Services offered
   - Attribute 4: Number of beds
   - Attribute 5: Number of uninsured patients
   - Media 1: Screenshot of website home or logo
   - Media 2: Map of location
   - List of closest hospitals *(connects to hospital instances)*
   - Data: [HRSA.gov](https://findahealthcenter.hrsa.gov/) (REST API), [HRSA.gov](https://data.hrsa.gov/data/download) (more data)

## What will our site answer?

1. What areas in the U.S. have the highest/lowest uninsured rates?
2. How can someone find free or low-cost healthcare in their area?
3. What hospitals are in my area and what is the price for the services they offer?