from sqlalchemy.orm import Session
from sqlalchemy import text
from dataclasses import asdict
import os
from sqlalchemy.sql.expression import or_

import hmac
import hashlib
import base64
import urllib.parse as urlparse

import models, schemas

import json

PUBLIC_GOOGLE_API_KEY = "AIzaSyBZ4vzaFSkL6jpna3ZAStHkKq81VJ_6CNE"
SIGNING_SECRET = os.getenv("SIGNING_SECRET")
PLACEHOLDER_IMAGE = "https://static.vecteezy.com/system/resources/thumbnails/025/621/796/small_2x/cute-medical-seamless-pattern-background-for-doctor-clinic-or-hospital-free-vector.jpg"

def get_state(db: Session, state_id: int):
    res = db.query(models.State).filter(models.State.id == state_id).first()
    res.topojson = db.execute(text(f"SELECT topojson FROM states WHERE id = {state_id}")).fetchone()[0]

    # get associated hospitals & organizations
    res.hospitals = db.query(models.Hospital).filter(
        models.Hospital.state_id == state_id and models.Hospital.url.isnot(None)).all()
    res.organizations = db.query(models.Organization).filter(
        models.Organization.state_id == state_id and models.Organization.url.isnot(None)).all()
    
    for hospital in res.hospitals:
        hospital.image = get_valid_image(hospital.image)
    for organization in res.organizations:
        organization.image = get_valid_image(organization.image)
    return res

def get_all_states(db: Session, skip: int = 0, limit: int = 10, **kwargs):
    res = db.query(models.State)

    # apply population filter
    if "pop_low" in kwargs:
        res = res.filter(models.State.population >= kwargs["pop_low"])
    if "pop_high" in kwargs:
        res = res.filter(models.State.population <= kwargs["pop_high"])

    # apply health center filter
    if "hc_low" in kwargs:
        res = res.filter(models.State.num_health_centers >= kwargs["hc_low"])
    if "hc_high" in kwargs:
        res = res.filter(models.State.num_health_centers <= kwargs["hc_high"])

    # apply hospital filter
    if "hos_low" in kwargs:
        res = res.filter(models.State.num_hospitals >= kwargs["hos_low"])
    if "hos_high" in kwargs:
        res = res.filter(models.State.num_hospitals <= kwargs["hos_high"])

    # apply uninsured filter
    if "uninsured_low" in kwargs:
        res = res.filter(models.State.percent_uninsured >= kwargs["uninsured_low"])
    if "uninsured_high" in kwargs:
        res = res.filter(models.State.percent_uninsured <= kwargs["uninsured_high"])

    # apply low income filter
    if "li_low" in kwargs:
        res = res.filter(models.State.percent_low_income >= kwargs["li_low"])
    if "li_high" in kwargs:
        res = res.filter(models.State.percent_low_income <= kwargs["li_high"])

    # apply sorting
    if "sort" in kwargs and kwargs["sort"] in ["population", "health_centers", "hospitals", "uninsured", "low_income", "name"]:
        order = "desc" if "order" in kwargs and kwargs["order"] == "desc" else "asc"
        if kwargs["sort"] == "uninsured":
            kwargs["sort"] = "percent_uninsured"
        if kwargs["sort"] == "low_income":
            kwargs["sort"] = "percent_low_income"
        if kwargs["sort"] == "health_centers":
            kwargs["sort"] = "num_health_centers"
        if kwargs["sort"] == "hospitals":
            kwargs["sort"] = "num_hospitals"
        sort = kwargs["sort"] + " " + order
        res = res.order_by(text(sort))

    total_count = res.count()
    res = res.offset(skip).limit(limit).all()
    return res, total_count

def get_hospital(db: Session, hospital_id: int):
    res = db.query(models.Hospital).filter(models.Hospital.id == hospital_id).first()

    # get associated organizations & state
    res.organizations = db.query(models.Organization).filter(
        (models.Organization.state_id == res.state_id) & (models.Organization.url.isnot(None))).all()
    for organization in res.organizations:
        organization.image = get_valid_image(organization.image)
    res.state = db.query(models.State).filter(models.State.id == res.state_id).first()

    # fill in image query with API key and sign with secret
    res.image = get_valid_image(res.image)

    return res

def get_all_hospitals(db: Session, skip: int = 0, limit: int = 10, **kwargs):
    res = db.query(models.Hospital).filter((models.Hospital.url.isnot(None)) & (models.Hospital.state_id.isnot(None)))

    # apply ownership filter
    ownership = {
        "gov_local": "Government - Local",
        "gov_state": "Government - State",
        "church": "Voluntary non-profit - Church",
        "physician": "Physician",
        "nonprofit": "Voluntary non-profit - Other",
        "private": "Voluntary non-profit - Private",
        "va": "Veterans Health Administration",
        "proprietary": "Proprietary",
        "gov_federal": "Government - Federal",
        "dod": "Department of Defense",
        "gov_hospital": "Government - Hospital District or Authority",
    }
    if "ownership" in kwargs:
        # parse into list if possible
        o = kwargs["ownership"]
        o = o.strip("[").strip("]").split(",")
        o = [i.strip() for i in o]

        try:
            res = res.filter(models.Hospital.ownership.in_(ownership[own] for own in o))
        except KeyError as e:
            print("Key error:", e)

    # apply type filter
    hospital_type = {
        "acute": "Acute Care Hospitals",
        "psych": "Psychiatric",
        "va": "Acute Care - Veterans Administration",
        "dod": "Acute Care - Department of Defense",
        "children": "Childrens",
    }
    if "type" in kwargs:
        # parse into list if possible
        t = kwargs["type"]
        t = t.strip("[").strip("]").split(",")
        t = [i.strip() for i in t]

        try:
            res = res.filter(models.Hospital.type.in_(hospital_type[typ] for typ in t))
        except KeyError as e:
            print("Key error:", e)

    # apply emergency_services filter
    if "emergency_services" in kwargs:
        res = res.filter(models.Hospital.emergency_services == "Yes")

    # apply rating filter
    if "rating_low" in kwargs:
        res = res.filter(models.Hospital.rating >= kwargs["rating_low"])
    if "rating_high" in kwargs:
        res = res.filter(models.Hospital.rating <= kwargs["rating_high"])

    # apply sorting
    if "sort" in kwargs and kwargs["sort"] in ["rating", "name"]:
        order = "desc" if "order" in kwargs and kwargs["order"] == "desc" else "asc"
        sort = kwargs["sort"] + " " + order
        res = res.order_by(text(sort))

    total_count = res.count()
    res = res.offset(skip).limit(limit).all()
    for hospital in res:
        hospital.image = get_valid_image(hospital.image)
    return res, total_count

def get_organization(db: Session, organization_id: int):
    res = db.query(models.Organization).filter(models.Organization.id == organization_id).first()

    # get associated hospitals & state
    res.hospitals = db.query(models.Hospital).filter(
        models.Hospital.state_id == res.state_id and models.Hospital.url.isnot(None)).all()
    for hospital in res.hospitals:
        hospital.image = get_valid_image(hospital.image)
    res.state = db.query(models.State).filter(models.State.id == res.state_id).first()

    # fill in image query with API key and sign with secret
    res.image = get_valid_image(res.image)
    return res

def get_all_organizations(db: Session, skip: int = 0, limit: int = 10, **kwargs):
    res = db.query(models.Organization).filter((models.Organization.url.isnot(None)) & (models.Organization.state_id.isnot(None)))

    # apply type filter
    organization_type = {
        "correctional": "Correctional Facility",
        "nursing": "Nursing Home",
        "school": "School",
        "hospital": "Hospital",
        "dv": "Domestic Violence",
        "other": "All Other Clinic Types"
    }
    if "type" in kwargs:
        # parse into list if possible
        t = kwargs["type"]
        t = t.strip("[").strip("]").split(",")
        t = [i.strip() for i in t]

        try:
            res = res.filter(models.Organization.type.in_(organization_type[typ] for typ in t))
        except KeyError as e:
            print("Key error:", e)

    # apply services filter # TODO fix this to parse correctly
    # possible options: medical, dental, mental, vision, enabling, substance
    if "services" in kwargs:
        s = kwargs["services"]
        s = s.strip("[").strip("]").split(",")
        s = [i.strip() for i in s]

        res = res.filter(or_(
            *[models.Organization.services.contains(service) for service in s]
        ))

    # apply num_patients_2022 filter
    if "np_low" in kwargs:
        res = res.filter(models.Organization.num_patients_2022 >= kwargs["np_low"])
    if "np_high" in kwargs:
        res = res.filter(models.Organization.num_patients_2022 <= kwargs["np_high"])

    # apply sorting
    if "sort" in kwargs and kwargs["sort"] in ["num_patients_2022", "name"]:
        order = "desc" if "order" in kwargs and kwargs["order"] == "desc" else "asc"
        sort = kwargs["sort"] + " " + order
        res = res.order_by(text(sort))
    
    total_count = res.count()
    res = res.offset(skip).limit(limit).all()
    for organization in res:
        organization.image = get_valid_image(organization.image)
    return res, total_count

def search_states(db: Session, query: str, limit: int = 20):    
    sql = f"""
    select *, ts_rank(search, to_tsquery('simple', '{query}:*')) rank
    from states where search @@ to_tsquery('simple', '{query}:*')
    order by rank desc
    limit {limit};
    """
    return db.query(models.State).from_statement(text(sql)).all()

def search_hospitals(db: Session, query: str, limit: int = 20):
    sql = f"""
    select *, ts_rank(search, to_tsquery('simple', '{query}:*')) rank
    from hospitals 
    where url is not null and state_id is not null and search @@ to_tsquery('simple', '{query}:*')
    order by rank desc
    limit {limit};
    """
    res = db.query(models.Hospital).from_statement(text(sql)).all()
    for hospital in res:
        hospital.image = get_valid_image(hospital.image)
    return res

def search_organizations(db: Session, query: str, limit: int = 20):
    sql = f"""
    select *, ts_rank(search, to_tsquery('simple', '{query}:*')) rank
    from organizations 
    where url is not null and state_id is not null and search @@ to_tsquery('simple', '{query}:*')
    order by rank desc
    limit {limit};
    """
    res = db.query(models.Organization).from_statement(text(sql)).all()
    for organization in res:
        organization.image = get_valid_image(organization.image)
    return res

def sign_url(input_url=None, secret=None):
    """ Sign a request URL with a URL signing secret.
      Usage:
      from urlsigner import sign_url
      signed_url = sign_url(input_url=my_url, secret=SECRET)
      Args:
      input_url - The URL to sign
      secret    - Your URL signing secret
      Returns:
      The signed request URL
    """

    if not input_url or not secret:
        raise Exception("Both input_url and secret are required")

    url = urlparse.urlparse(input_url)
    url_to_sign = url.path + "?" + url.query
    decoded_key = base64.urlsafe_b64decode(secret)
    signature = hmac.new(decoded_key, str.encode(url_to_sign), hashlib.sha1)
    encoded_signature = base64.urlsafe_b64encode(signature.digest())

    original_url = url.scheme + "://" + url.netloc + url.path + "?" + url.query
    return original_url + "&signature=" + encoded_signature.decode()

def get_valid_image(image: str):
    if not image:
        return PLACEHOLDER_IMAGE
    if "None" in image:
        return PLACEHOLDER_IMAGE
    else:
        return sign_url(f"{image}&key={PUBLIC_GOOGLE_API_KEY}", SIGNING_SECRET)



### VISUALIZATIONS ###
def num_per_state(db: Session):
    # get number of hospitals and organizations per state
    q = """
    SELECT states.id, states.name,
    COUNT(DISTINCT hospitals.id) as num_hospitals,
    COUNT(DISTINCT organizations.id) as num_organizations
    FROM states
    LEFT JOIN hospitals ON states.id = hospitals.state_id
    LEFT JOIN organizations ON states.id = organizations.state_id
    GROUP BY states.id, states.name
    """
    res = db.execute(text(q)).fetchall()
    res = [{
        "id": row[0],
        "name": row[1],
        "num_hospitals": row[2],
        "num_organizations": row[3]
    } for row in res]
    return res

def dems_per_state(db: Session):
    # get a list of all states with a list of all percent uninsured by county
    q = """
    SELECT states.id, states.name, counties.percent_uninsured
    FROM states
    LEFT JOIN counties ON states.id = counties.state_id
    """
    res = db.execute(text(q)).fetchall()
    res = [{
        "id": row[0],
        "name": row[1],
        "percent_uninsured": row[2]
    } for row in res]
    # condense into a list of states with a list of percent uninsured
    states = {}
    for row in res:
        if row["id"] not in states:
            states[row["id"]] = {
                "name": row["name"],
                "percent_uninsured": []
            }
        states[row["id"]]["percent_uninsured"].append(row["percent_uninsured"])
    return [states[state] for state in states]

def hospital_orgs_types(db: Session):
    m = {
        "Acute Care Hospitals": "Acute Care",
        "Psychiatric": "Psychiatric",
        "Critical Access Hospitals": "Critical Access",
        "Childrens": "Childrens",
        "Acute Care - Department of Defense": "Acute Care DoD",
        "All Other Clinic Types": "Other",
        "Acute Care - Veterans Administration": "Acute Care VA",
        "Nursing Home": "Nursing Home",
        "Correctional Facility": "Correctional Facility",
        "School": "School",
        "Hospital": "Hospital",
        "Domestic Violence": "Domestic Violence",
    }
    # get a count of hospitals and organizations by type
    q = """
    SELECT hospitals.type, COUNT(hospitals.id) as num_hospitals
    FROM hospitals
    GROUP BY hospitals.type
    """
    res = db.execute(text(q)).fetchall()
    hospitals = [{
        "name": m[row[0]],
        "value": row[1]
    } for row in res if row[1] > 50]

    q = """
    SELECT organizations.type, COUNT(organizations.id) as num_organizations
    FROM organizations
    GROUP BY organizations.type
    """

    res = db.execute(text(q)).fetchall()
    organizations = [{
        "name": m[row[0]],
        "value": row[1]
    } for row in res if row[1] > 50]

    # create a dictionary with the counts
    # {"children":["name": "hospitals", "children": [{"name": "Acute Care Hospitals", "value": 100}]}
    res = {
        "name": "types",
        "children": [
            {
                "name": "hospitals",
                "children": hospitals
            },
            {
                "name": "organizations",
                "children": organizations
            }
        ]
    }

    return res

