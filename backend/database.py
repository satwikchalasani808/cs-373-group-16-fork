from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, declarative_base
import os

DB_URL = os.getenv("DATABASE_URL")

SQLALCHEMY_DATABASE_URL = f"postgresql://{DB_URL if DB_URL else 'postgres:abcd1234@care-hub.chq8u2eiyctk.us-east-2.rds.amazonaws.com'}:5432/postgres"

engine = create_engine(SQLALCHEMY_DATABASE_URL)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
Base = declarative_base()