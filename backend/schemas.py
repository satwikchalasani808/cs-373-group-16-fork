from pydantic import BaseModel
from typing import Optional, Any, Dict, List, ForwardRef

StateBase = ForwardRef("StateBase")
HospitalBase = ForwardRef("HospitalBase")
OrganizationBase = ForwardRef("OrganizationBase")

class StateBase(BaseModel):
    id: int
    name: str
    population: int
    num_health_centers: Optional[int] = None
    num_hospitals: Optional[int] = None
    percent_uninsured: float
    percent_low_income: float
    image: str

class County(BaseModel):
    id: int
    name: str
    percent_uninsured: float
    fips: int

class State(StateBase):
    description: str
    topojson: Any
    counties: List[County]
    hospitals: List[HospitalBase]
    organizations: List[OrganizationBase]

    class Config:
        orm_mode = True

class HospitalBase(BaseModel):
    id: int
    city: str
    state_abbr: str
    name: str
    ownership: str
    type: str
    emergency_services: str
    rating: Optional[int] = None
    image: Optional[str] = None # TODO make this required
    # image: str

class Hospital(HospitalBase):
    description: Optional[str] = None # TODO make this required
    map: Optional[str] = None # TODO make this required
    address: str
    url: Optional[str] = None # TODO make this required
    state: StateBase
    organizations: List[OrganizationBase]

    class Config:
        orm_mode = True

class OrganizationBase(BaseModel):
    id: int
    num_patients_2022: Optional[int] = None
    services: Optional[str] = ""
    name: str
    city: str
    state_abbr: str
    image: Optional[str] = None # TODO make this required
    type: str
    # image: str

class Organization(OrganizationBase):
    description: Optional[str] = None # TODO make this required
    map: Optional[str] = None # TODO make this required
    address: str
    url: Optional[str] = None # TODO make this required
    state: StateBase
    hospitals: List[HospitalBase]

    class Config:
        orm_mode = True