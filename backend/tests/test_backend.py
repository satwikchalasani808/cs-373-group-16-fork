import pytest
from fastapi.testclient import TestClient
from main import app
import os

client = TestClient(app)

def test_endpoint_1():
    response =  client.get("http://api.care-hub.me/states")
    assert response.status_code == 200

def test_endpoint_2():
    response =  client.get("http://api.care-hub.me/states/1")  
    assert response.status_code == 200

def test_endpoint_3():
    assert os.environ['SIGNING_SECRET']
    response =  client.get("http://api.care-hub.me/hospitals")  
    assert response.status_code == 200

def test_endpoint_4():
    assert os.environ['SIGNING_SECRET']
    response =  client.get("http://api.care-hub.me/hospitals/5")  
    assert response.status_code == 200

def test_endpoint_5():
    assert os.environ['SIGNING_SECRET']
    response =  client.get("http://api.care-hub.me/organizations")  #
    assert response.status_code == 200

def test_endpoint_6():
    assert os.environ['SIGNING_SECRET']
    response =  client.get("http://api.care-hub.me/organizations/16605")  
    assert response.status_code == 200