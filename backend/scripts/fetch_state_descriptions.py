from requests_html import HTMLSession
from bs4 import BeautifulSoup
from readability import Document
import psycopg2

headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/110.0'}
db_host = "localhost"
db_port = "5432"
db_name = "postgres"

def parse_readability(html: str):
    doc = Document(html)
    page_soup = BeautifulSoup(doc.summary(), 'html.parser')
    return page_soup.find('p').text if page_soup.find('p') else None

def fetch_healthcare_paragraph(state):
    query = f"healthcare in {state}"
    search_url = f"https://www.google.com/search?q={query}"

    # Send a GET request to Google Search
    session = HTMLSession()
    response = session.get(search_url, headers=headers)
    response.html.render(sleep=2)

    # Parse HTML content using BeautifulSoup
    soup = BeautifulSoup(response.text, 'html.parser')

    # Find the first search result link
    links = soup.find('div', {'id': 'search'})
    links = [a.get('href') for a in links.find_all('a')]

    for link in links:
        if not link:
            continue
        if not link.startswith('http'):
            continue
        # print(f"Trying link: {link}")

        # Send a GET request to the first search result page
        page_response = session.get(link, headers=headers)
        page_response.html.render(sleep=1)

        # try finding main content
        page_soup = BeautifulSoup(page_response.text, 'html.parser')
        main_content = page_soup.find('main')
        if main_content:
            curr_p = main_content.find('p')
            content = curr_p.text
            while len(content) < 200:
                curr_p = curr_p.find_next('p')
                content = curr_p.text
            if content and len(content) > 200:
                return content, link
        
        # try using readability
        # content = parse_readability(page_response.text)
        # if content and len(content) > 200:
        #     return content
        
    return None, None


def update_database(images_data):
    connection = psycopg2.connect(
        host=db_host,
        port=db_port,
        dbname=db_name
    )

    cursor = connection.cursor()

    for state, desc in images_data.items():
        desc = desc.replace("'","''");
        update_query = f"UPDATE states SET description = '{desc}' WHERE name = '{state}'"
        cursor.execute(update_query)

    connection.commit()
    cursor.close()
    connection.close()

def get_states():
    connection = psycopg2.connect(
        host=db_host,
        port=db_port,
        dbname=db_name
    )

    cursor = connection.cursor()
    cursor.execute("SELECT name FROM states WHERE description IS NULL")
    states = cursor.fetchall()
    cursor.close()
    connection.close()

    for i in range(len(states)):
        states[i] = states[i][0]
    return states


if __name__ == "__main__":
    states = get_states()
    with open('state_desc_sources.txt', 'w') as f:
        desc_data = {}
        for state in states:
            try:
                desc, link = fetch_healthcare_paragraph(state)
                desc_data[state] = desc
                f.write(f"{state}: {link}\n")
                print(f"Got description for {state}")
            except:
                print(f"Failed to get description for {state}")
        update_database(desc_data)