import psycopg2

def update_state_id_col(db_connection_string):

    conn = psycopg2.connect(db_connection_string)
    cur = conn.cursor()

    query = "SELECT id, name FROM states;"
    cur.execute(query)
    states = cur.fetchall()

    for state in states:
        state_id = state[0]
        state_name = state[1]
        query = f"UPDATE counties SET state_id = {state_id} WHERE state = '{state_name}';"
        cur.execute(query)

        print(f"Updated state_id for {state_name}.")

    # Close the database connection
    conn.commit()
    cur.close()
    conn.close()

def main():
    db_connection_string = 'postgresql://localhost:5432/postgres'

    # Call the function to filter counties by state and save TopoJSON to PostgreSQL
    update_state_id_col(db_connection_string)

    print("Processing completed.")

if __name__ == "__main__":
    main()
