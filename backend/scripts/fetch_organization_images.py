import requests
from urllib.parse import urlparse, parse_qs

from dotenv import load_dotenv
import os
import psycopg2

# Load environment variables from .env file
load_dotenv()

headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/110.0'}
db_host = "care-hub.chq8u2eiyctk.us-east-2.rds.amazonaws.com"
db_port = "5432"
db_name = "postgres"
db_user = "postgres"
db_password = "abcd1234"

connection = psycopg2.connect(
            host=db_host,
            port=db_port,
            dbname=db_name,
            user=db_user,
            password=db_password
        )
cursor = connection.cursor()

import signal
def handler(signum, frame):
    connection.commit()
    cursor.close()
    connection.close()

signal.signal(signal.SIGTSTP, handler)


def fetch_organization_images(address, city, state_abbr):
    try:
        geocode_response = requests.get(f"https://maps.googleapis.com/maps/api/geocode/json?address={address + ', ' + city + ', ' + state_abbr}&key={os.getenv('PUBLIC_GOOGLE_API_KEY')}")
        geocode_data = geocode_response.json()

        if geocode_data['status'] == 'OK':
            lat = geocode_data['results'][0]['geometry']['location']['lat']
            lng = geocode_data['results'][0]['geometry']['location']['lng']
            return f"https://maps.googleapis.com/maps/api/streetview?size=640x640&location={lat},{lng}"
            street_view_response = requests.get(f"https://maps.googleapis.com/maps/api/streetview?size=640x640&location={lat},{lng}&key={os.getenv('PUBLIC_GOOGLE_API_KEY')}")
            return street_view_response.content
        else:
            print('Geocoding failed:', geocode_data['status'])
    except Exception as e:
        print('Error:', e)


if __name__=="__main__":
    # zip 36301
    # image_data = fetch_hospital_images('1110 ROSS CLARK CIRCLE', 'DOTHAN', 'AL')
    # print(image_data)

    try:
        cursor.execute("SELECT address, city, state_abbr FROM organizations WHERE image IS NULL AND url IS NOT NULL ORDER BY random() LIMIT 20")
        links = cursor.fetchall()

        while links:
            for i, link in enumerate(links):
                address, city, state_abbr = link
                try: 
                    image = fetch_organization_images(address, city, state_abbr)
                except:
                    print(f"{i}/{len(links)}: Failed to fetch for {link}")
                    continue
                if not image:
                    print(f"{i}/{len(links)}: No image for {link}")
                    continue
                cursor.execute("UPDATE organizations SET image = %s WHERE address = %s AND city = %s AND state_abbr = %s", (image, address, city, state_abbr))
                print(f"{i}/{len(links)}: Updated {link} with image")
            connection.commit()
            cursor.execute("SELECT address, city, state_abbr FROM organizations WHERE image IS NULL AND url IS NOT NULL ORDER BY random() LIMIT 20")
            links = cursor.fetchall()
          
        cursor.close()
        connection.close()
    except KeyboardInterrupt:
        connection.commit()
        cursor.close()
        connection.close()