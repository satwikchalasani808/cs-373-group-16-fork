state_abbreviations = {
    "AL": "Alabama",
    "AK": "Alaska",
    "AZ": "Arizona",
    "AR": "Arkansas",
    "CA": "California",
    "CO": "Colorado",
    "CT": "Connecticut",
    "DE": "Delaware",
    "FL": "Florida",
    "GA": "Georgia",
    "HI": "Hawaii",
    "ID": "Idaho",
    "IL": "Illinois",
    "IN": "Indiana",
    "IA": "Iowa",
    "KS": "Kansas",
    "KY": "Kentucky",
    "LA": "Louisiana",
    "ME": "Maine",
    "MD": "Maryland",
    "MA": "Massachusetts",
    "MI": "Michigan",
    "MN": "Minnesota",
    "MS": "Mississippi",
    "MO": "Missouri",
    "MT": "Montana",
    "NE": "Nebraska",
    "NV": "Nevada",
    "NH": "New Hampshire",
    "NJ": "New Jersey",
    "NM": "New Mexico",
    "NY": "New York",
    "NC": "North Carolina",
    "ND": "North Dakota",
    "OH": "Ohio",
    "OK": "Oklahoma",
    "OR": "Oregon",
    "PA": "Pennsylvania",
    "RI": "Rhode Island",
    "SC": "South Carolina",
    "SD": "South Dakota",
    "TN": "Tennessee",
    "TX": "Texas",
    "UT": "Utah",
    "VT": "Vermont",
    "VA": "Virginia",
    "WA": "Washington",
    "WV": "West Virginia",
    "WI": "Wisconsin",
    "WY": "Wyoming"
}

import psycopg2

db_host = "localhost"
db_port = "5432"
db_name = "postgres"

connection = psycopg2.connect(
            host=db_host,
            port=db_port,
            dbname=db_name
        )
cursor = connection.cursor()

# get the state id for each state in the states table
# and put it in the state_id column in the hospitals table

for state_abbr, state_name in state_abbreviations.items():
    cursor.execute("SELECT id FROM states WHERE name = %s", (state_name,))
    state_id = cursor.fetchone()[0]
    cursor.execute("UPDATE hospitals SET state_id = %s WHERE state_abbr = %s", (state_id, state_abbr))
    print(f"Updated state_id for {state_name}")
    connection.commit()

cursor.close()
connection.close()