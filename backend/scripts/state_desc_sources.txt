Alabama: https://www.alabamapublichealth.gov/healthrankings/access-to-care.html
Alaska: https://www.commerce.alaska.gov/web/ins/Consumers/Health.aspx
Arizona: https://www.linkedin.com/company/ahcccs
Arkansas: http://www.arkansasbluecross.com/
Colorado: https://connectforhealthco.com/
Connecticut: https://portal.ct.gov/Services/Health-and-Human-Services/Healthcare-and-Insurance/Health-Insurance
Delaware: https://www.choosehealthde.com/Health-Insurance-Marketplace
Florida: https://www.fhcp.com/
Idaho: https://healthandwelfare.idaho.gov/
Illinois: https://hfs.illinois.gov/
Indiana: https://www.in.gov/fssa/hip/
Maine: http://www.mainehealth.org/
Maryland: https://www.maryland.gov/pages/residents.aspx?view=Health%20and%20Wellness&subcat=Health%20Care
Michigan: https://www.michigan.gov/mdhhs/assistance-programs/healthcare
Minnesota: https://www.health.state.mn.us/facilities/insurance/clearinghouse/clhlthcov.html
Mississippi: https://msdh.ms.gov/page/44,0,236.html
Montana: https://healthinfo.montana.edu/workforce-development/past-projects/healthcare-mt.html
Nevada: https://www.nevadahealthlink.com/
New Jersey: https://www.nj.gov/getcoverednj/
New Mexico: https://www.ruralhealthinfo.org/states/new-mexico
North Carolina: https://www.ncdhhs.gov/
North Dakota: https://ruralhealth.und.edu/projects/flex/hospitals
Oklahoma: https://oklahoma.gov/ohca.html
Oregon: https://healthcare.oregon.gov/pages/types-of-coverage.aspx
Rhode Island: https://www.lifespan.org/
Tennessee: https://www.healthinsurance.org/states/health-insurance-tennessee/
Texas: https://www.hhs.texas.gov/services/health
Utah: https://healthcare.utah.edu/
Vermont: http://info.healthconnect.vermont.gov/home-page
Virginia: http://www.marketplace.virginia.gov/
Washington: https://www.hca.wa.gov/
West Virginia: https://www.ruralhealthinfo.org/states/west-virginia
Wyoming: https://www.healthinsurance.org/states/health-insurance-wyoming/
