from requests_html import HTMLSession
from bs4 import BeautifulSoup
import psycopg2

headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/110.0'}
db_host = "localhost"
db_port = "5432"
db_name = "postgres"

def get_image_for_state(state: str) -> str:
    state = state.lower()
    search_url = f'https://unsplash.com/s/photos/{state}?license=free&orientation=landscape'
    session = HTMLSession()
    response = session.get(search_url, headers=headers)
    response.html.render(sleep=2)
    soup = BeautifulSoup(response.html.html, 'html.parser')
    # print(soup.prettify())
    images = soup.find_all('img', recursive=True)
    # print(images)
    for im in images:
    #     im_resolutions = im['srcset'].split(',')
    #     res1000 = im_resolutions.find('1000w')
    #     if res1000:
    #         print(res1000.split(' ')[0])
    # for i in img:
        if 'unsplash' in im['src']:
            return im['src']
        
def update_database(images_data):
    connection = psycopg2.connect(
        host=db_host,
        port=db_port,
        dbname=db_name
    )

    cursor = connection.cursor()

    for state, image_url in images_data.items():
        update_query = f"UPDATE states SET image = '{image_url}' WHERE name = '{state}'"
        cursor.execute(update_query)

    connection.commit()
    cursor.close()
    connection.close()

def get_states():
    connection = psycopg2.connect(
        host=db_host,
        port=db_port,
        dbname=db_name
    )

    cursor = connection.cursor()
    cursor.execute("SELECT name FROM states")
    states = cursor.fetchall()
    cursor.close()
    connection.close()

    for i in range(len(states)):
        states[i] = states[i][0]
    return states


if __name__=="__main__":
    states = get_states()
    images_data = {}
    for state in states:
        try:
            images_data[state] = get_image_for_state(state)
        except:
            print(f"Failed to get image for {state}")
    update_database(images_data)