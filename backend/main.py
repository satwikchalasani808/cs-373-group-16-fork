from fastapi import FastAPI, Depends, HTTPException, Request
from sqlalchemy.orm import Session

from fastapi.middleware.cors import CORSMiddleware

import models, schemas, crud
from database import SessionLocal, engine

import json

models.Base.metadata.create_all(bind=engine)

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],  # Replace with your frontend URL(s) in production
    allow_credentials=True,
    allow_methods=["GET", "POST", "PUT", "DELETE"],
    allow_headers=["*"],
)

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

@app.get("/states/{state_id}", response_model=schemas.State)
def read_state(state_id: int, db: Session = Depends(get_db)):
    db_state = crud.get_state(db, state_id)
    if db_state is None:
        raise HTTPException(status_code=404, detail="State not found")
    return db_state

@app.get("/states/", response_model=tuple[list[schemas.StateBase], int])
def read_states(skip: int = 0, limit: int = 10, db: Session = Depends(get_db), request: Request = None):
    kwargs = request.query_params._dict
    kwargs.pop("skip", None)
    kwargs.pop("limit", None)
    states, count = crud.get_all_states(db, skip=skip, limit=limit, **kwargs)
    return states, count

@app.get("/hospitals/{hospital_id}", response_model=schemas.Hospital)
def read_hospital(hospital_id: int, db: Session = Depends(get_db)):
    db_hospital = crud.get_hospital(db, hospital_id)
    if db_hospital is None:
        raise HTTPException(status_code=404, detail="Hospital not found")
    return db_hospital

@app.get("/hospitals/", response_model=tuple[list[schemas.HospitalBase], int])
def read_hospitals(skip: int = 0, limit: int = 10, db: Session = Depends(get_db), request: Request = None):
    kwargs = request.query_params._dict
    kwargs.pop("skip", None)
    kwargs.pop("limit", None)
    hospitals, count = crud.get_all_hospitals(db, skip=skip, limit=limit, **kwargs)
    return hospitals, count

@app.get("/organizations/{organization_id}", response_model=schemas.Organization)
def read_organization(organization_id: int, db: Session = Depends(get_db)):
    db_organization = crud.get_organization(db, organization_id)
    if db_organization is None:
        raise HTTPException(status_code=404, detail="Organization not found")
    return db_organization

@app.get("/organizations/", response_model=tuple[list[schemas.OrganizationBase], int])
def read_organizations(skip: int = 0, limit: int = 10, db: Session = Depends(get_db), request: Request = None):
    kwargs = request.query_params._dict
    kwargs.pop("skip", None)
    kwargs.pop("limit", None)
    organizations, count = crud.get_all_organizations(db, skip=skip, limit=limit, **kwargs)
    return organizations, count

@app.get("/search/states", response_model=list[schemas.StateBase])
def search_states(query: str, db: Session = Depends(get_db)):
    states = crud.search_states(db, query)
    return states

@app.get("/search/hospitals", response_model=list[schemas.HospitalBase])
def search_hospitals(query: str, db: Session = Depends(get_db)):
    hospitals = crud.search_hospitals(db, query)
    return hospitals

@app.get("/search/organizations", response_model=list[schemas.OrganizationBase])
def search_organizations(query: str, db: Session = Depends(get_db)):
    organizations = crud.search_organizations(db, query)
    return organizations

@app.get("/search", response_model=dict)
def search(query: str, db: Session = Depends(get_db)):
    states = crud.search_states(db, query, limit=10)
    states = [schemas.StateBase(**state.__dict__) for state in states]
    hospitals = crud.search_hospitals(db, query, limit=10)
    hospitals = [schemas.HospitalBase(**hospital.__dict__) for hospital in hospitals]
    organizations = crud.search_organizations(db, query, limit=10)
    organizations = [schemas.OrganizationBase(**organization.__dict__) for organization in organizations]
    return {"states": states, "hospitals": hospitals, "organizations": organizations}


### VISUALIZATIONS ###
@app.get("/visualizations/totals", response_model=None)
def get_total(db: Session = Depends(get_db)):
    return crud.num_per_state(db)

@app.get("/visualizations/dems", response_model=None)
def get_dems(db: Session = Depends(get_db)):
    return crud.dems_per_state(db)

@app.get("/visualizations/types", response_model=None)
def get_types(db: Session = Depends(get_db)):
    return crud.hospital_orgs_types(db)

if __name__ == "__main__":
    import uvicorn
    uvicorn.run("main:app", host="0.0.0.0", reload=True)