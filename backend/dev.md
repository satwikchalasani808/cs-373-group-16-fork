To start, run `initdb db` from the **backend** folder. This will set up the database.

You can view the contents of the DB using a UI tool like DBeaver or pgAdmin.

Use `pg_ctl -D db start` to start the server.